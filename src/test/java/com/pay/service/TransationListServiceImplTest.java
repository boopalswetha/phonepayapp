package com.pay.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.pay.Exception.TransactionNotFoundException;
import com.pay.Exception.UserNotfoundException;
import com.pay.Model.User;
import com.pay.Service.TransacServiceImpl;
import com.pay.Service.UserServiceImpl;
import com.pay.repository.UserRepository;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TransationListServiceImplTest {
	@InjectMocks
	UserServiceImpl transationListServiceImpl;
	@Mock
	UserRepository userRepository;
	static User user = null;
	@BeforeClass
	public static void setUp() {
		user = new User();
	}
	@Test(expected = NullPointerException.class)
	public void testgettransationlistsPositive() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setName("boopal");
		user.setGender("male");
		user.setPhone("8838662182");
		user.setDOB(null);
		users.add(user);
		Mockito.when(userRepository.findByPhone("8838662182")).thenReturn(null);
		User userss =  transationListServiceImpl.getuserlists("8838662182");
		Assert.assertNotNull(userss);
        
	}
	@Test
	public void testgettransationlistsNagative() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setName("boopal");
		user.setGender("male");
		user.setPhone("-8838662182");
		user.setDOB(null);
		users.add(user);
		Mockito.when(userRepository.findByPhone("8838662182")).thenReturn(Optional.of(user));
		User userss =  transationListServiceImpl.getuserlists("8838662182");
		Assert.assertNotNull(userss);
        ;
	}
	@Test(expected = UserNotfoundException.class)
	public void testfindByPhoneForPositive1() {
		List<User> users = new ArrayList();
		User user = new User();
		user.setPhone("-8838662188");
		user.setDOB(null);
		user.setName("boopal");
		users.add(user);
	//	Mockito.when(userRepository.findByPhone("8838662182")).thenReturn(Optional.of(user));
	User	 usesss =  transationListServiceImpl.getuserlists(null);
		Assert.assertNotNull(usesss);
		
	}

}
