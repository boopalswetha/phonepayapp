package com.pay.Service;

import com.pay.Dto.TransactionDto;
import com.pay.Dto.TransactionResponseDto;


public interface TransacService {

	TransactionResponseDto createTransac(TransactionDto transactionDto);

}
