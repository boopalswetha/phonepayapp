package com.pay.Service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.pay.Dto.ResponseDto;
import com.pay.Dto.UserDto;
import com.pay.Exception.UserNotfoundException;
import com.pay.Model.User;

import com.pay.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	UserRepository userRepository;
	

	
	public String getByPhoneNo(String phone) {

		String url = "http://localhost:8081/user/" + phone;
		try {
			String result = restTemplate.getForObject(url, String.class);
			return result;
		} catch (Exception e) {
			throw new UserNotfoundException("requested user not  there in bank servers");
		}
	}

	@Override
	public ResponseDto createUser(UserDto  userDto) {
	String s=	this.getByPhoneNo(userDto.getPhone());
	User user =new User();
	BeanUtils.copyProperties(userDto, user);
		//System.out.println(s);
		if(s!=null) {
			
		try {
			userRepository.save(user);
		} catch (Exception e) {
			throw new UserNotfoundException("user already exits in phonepay Database");
		}
		ResponseDto res =new ResponseDto();
		res.setMessage("your phonePay Account  is Created sucessFully ");
		return res;
		}
		else {
			throw new UserNotfoundException("user is not there");
		}
	}
	
	@Override
	public User getuserlists(String phone) {
		Optional<User> user = userRepository.findByPhone(phone);
		User users = null;
		if(user.isPresent()) {
			users= user.get();
		}else
		{
			throw new UserNotfoundException("This transation phonenumber:" +phone+ "Not Found");
		}
		return users;
	}
	
	

}
