package com.pay.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.pay.Dto.TransactionDto;
import com.pay.Dto.TransactionResponseDto;

import com.pay.Service.TransacService;

@RestController
public class TransactionController {
	@Autowired
	TransacService transactionservice;
	
	@PostMapping("/payment")
	public ResponseEntity<TransactionResponseDto> transaction(@RequestBody TransactionDto transactionDto) {
		
		TransactionResponseDto transaction=transactionservice.createTransac(transactionDto);
		
		return new ResponseEntity<>(transaction, HttpStatus.OK);
	}
	

}
