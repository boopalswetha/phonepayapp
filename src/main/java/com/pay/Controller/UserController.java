package com.pay.Controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pay.Dto.ResponseDto;
import com.pay.Dto.UserDto;
import com.pay.Model.User;
import com.pay.Service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;

	@PostMapping("user")
	public ResponseEntity<ResponseDto> createUser(@RequestBody @Valid UserDto userDto) throws Exception {
		ResponseDto responseDto = userService.createUser(userDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@GetMapping(value = "/user/{phone}")
	public ResponseEntity<User> getuser(@PathVariable("phone") String phone) {
		User user = userService.getuserlists(phone);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}