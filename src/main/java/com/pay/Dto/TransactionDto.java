package com.pay.Dto;

public class TransactionDto {
	
	private String phoneNo;
	private String toPhoneNo;
	private double amount;
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getToPhoneNo() {
		return toPhoneNo;
	}
	public void setToPhoneNo(String toPhoneNo) {
		this.toPhoneNo = toPhoneNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	

}
