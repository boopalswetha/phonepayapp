package com.pay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pay.Model.Transaction;
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
